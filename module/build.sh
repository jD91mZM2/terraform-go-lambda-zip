#!/usr/bin/env bash

set -euo pipefail

output="$(mktemp -u)"

cd "$GO_MODULE_DIR"

go build -o "$output"

go run github.com/aws/aws-lambda-go/cmd/build-lambda-zip -o lambda.zip "$output"

rm "$output"
