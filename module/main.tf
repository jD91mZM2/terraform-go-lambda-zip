variable go_module_dir {}

resource null_resource fetch_dependencies {
  provisioner "local-exec" {
    command = "go get github.com/aws/aws-lambda-go/cmd/build-lambda-zip"
    working_dir = path.module
  }
}

resource null_resource build_lambda_zip {
  triggers = {
    # This should probably be changed to a hash of all Go source files
    always_trigger = timestamp()
  }

  depends_on = [ null_resource.fetch_dependencies ]

  provisioner "local-exec" {
    command = "./build.sh"
    working_dir = path.module
    environment = {
      GO_MODULE_DIR = var.go_module_dir
    }
  }
}

output lambda_zip_path {
  value = format("%s/%s", var.go_module_dir, "lambda.zip")
}
