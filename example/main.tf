module build_go {
  source = "../module"

  go_module_dir = abspath("./function")
}

output zip_path {
  value = module.build_go.lambda_zip_path
}
